# libra-proxy

#### 项目介绍
微服务跨IDC机房代理服务
可以将异地机房的服务互相代理注册

#### 软件架构
<img src="https://s1.ax1x.com/2018/04/25/C1936I.jpg"/>
libra-proxy-web 代理web服务<br/>

libra-proxy-service 代理service服务<br/>
配置proxy.properties
```Java
proxy.http={"proxy":"false","host":"192.168.3.101","port":"802"}
proxy.rules=[{"proxyServer":"http://localhost:8921/libra-proxy-web/","apps":["libra-demo-consumer"]}]
```
proxyServer 代理web服务地址<br/>
apps 所代理的服务名称<br/>
