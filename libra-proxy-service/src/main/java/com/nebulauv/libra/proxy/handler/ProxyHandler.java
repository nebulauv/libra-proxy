package com.nebulauv.libra.proxy.handler;


import com.nebulauv.libra.common.util.HessianSerialize;
import com.nebulauv.libra.config.LibraConfig;
import com.nebulauv.libra.proxy.support.util.http.HttpClientUtil;
import com.nebulauv.libra.register.zookeeper.LibraZk;
import com.nebulauv.libra.remoteing.netty.data.RpcRequest;
import com.nebulauv.libra.remoteing.netty.data.RpcResponse;
import org.apache.commons.collections.map.HashedMap;

import java.util.Map;

/**
 * Created by zhanghui on 2018/2/8.
 */
public class ProxyHandler {

    public RpcResponse send(RpcRequest request) throws Exception{

        String className= request.getClassName();
        //String methodName= request.getMethodName();
        //String version= request.getVersion();
        //从zk中找到相应的URL  待实现
        String proxyPath=getProxyNodePath(className);
        String url= HessianSerialize.deserialize(LibraZk.INSTANCE.client().getData().forPath(proxyPath),String.class);
        url=url+"/rpc/proxy-service";

        Map<String,String> map=new HashedMap();
        map.put("data", HessianSerialize.serializeStr(request));
        RpcResponse rpcResponse= HttpClientUtil.rpcPost(url,map);
        return  rpcResponse;
    }

    public  String getProxyNodePath(String service) {
        return String.format("/proxy/%s", service);
    }
}
