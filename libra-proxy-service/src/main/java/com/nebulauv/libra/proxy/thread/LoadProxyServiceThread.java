package com.nebulauv.libra.proxy.thread;

import com.nebulauv.libra.common.util.HessianSerialize;
import com.nebulauv.libra.common.util.encoder.JsonEncoder;
import com.nebulauv.libra.config.LibraConfig;
import com.nebulauv.libra.proxy.model.ProxyRulesBean;
import com.nebulauv.libra.proxy.support.constant.Constant;
import com.nebulauv.libra.proxy.support.util.ProxyPropertiesUtil;
import com.nebulauv.libra.proxy.support.util.http.HttpClientUtil;
import com.nebulauv.libra.register.data.RpcProvider;
import com.nebulauv.libra.register.zookeeper.LibraZk;
import com.nebulauv.libra.register.zookeeper.ZkDiscovery;
import com.nebulauv.libra.register.zookeeper.ZkRegistry;
import org.apache.commons.collections.map.HashedMap;
import org.apache.zookeeper.data.Stat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by zhanghui on 2018/2/9.
 */
public class LoadProxyServiceThread implements Runnable {
    private static final Logger logger = LoggerFactory.getLogger(LoadProxyServiceThread.class);

    private static final List<ProxyRulesBean>  PROXY_RULES_BEAN_LIST =new ArrayList<>();

    static {
       String proxyRulesStr= ProxyPropertiesUtil.getProperty(Constant.PROXY_RULES);
        List<ProxyRulesBean> list= JsonEncoder.DEFAULT.decode(proxyRulesStr,List.class,ProxyRulesBean.class);
        for (ProxyRulesBean bean: list){
            PROXY_RULES_BEAN_LIST.add(bean);
        }
    }
    @Override
    public void run() {

        List<RpcProvider> rpcProviderList=new ArrayList<>();
        for (ProxyRulesBean  proxyRulesBean :PROXY_RULES_BEAN_LIST) {
           /* List<String> appNameList=proxyRulesBean.getApps();
            for (String appName: appNameList) {
                Map<String,String> map=new HashedMap();
                String url=proxyRulesBean.getProxyServer()+"/rpc/get/"+appName+"/service";
                List<RpcProvider> list= HttpClientUtil.doPost(url,map,List.class );


                for (RpcProvider rpcProvider :list) {
                    rpcProvider.setAddress(LibraConfig.INSTANCE.getServerAddress().getAddress());
                    rpcProvider.setDesc("PROXY-FROM:"+proxyRulesBean.getProxyServer()+"");

                    try{
                        RpcProvider local= ZkDiscovery.INSTANCE.discover(rpcProvider.getInterfaceName(),rpcProvider.getVersion());
                        if (null==local){//当本地无此服务的时候，进行注册
                            rpcProviderList.add(rpcProvider);

                            String proxyPath=getProxyNodePath(rpcProvider.getInterfaceName());
                            logger.info("保存代理服务URL临时节点 {}",proxyPath);
                            LibraZk.INSTANCE.createNode(proxyPath, true);
                            LibraZk.INSTANCE.client().setData().forPath(proxyPath, HessianSerialize.serialize(proxyRulesBean.getProxyServer()));
                        }else {
                            logger.error("代理服务{}在本地已经注册过，不能注册为远程代理服务",rpcProvider.getInterfaceName());
                        }
                    }catch (Exception e){
                        logger.error("发现服务{}异常e:{}",rpcProvider.getInterfaceName(),e.getMessage());
                    }

                }

            }*/
        }

        //动态下线之前代理的服务
        String proxyPath="/libra/proxys";
        List<RpcProvider> oldList= null;
        try {
            Stat stat = LibraZk.INSTANCE.client().checkExists().forPath(proxyPath);
            if (null!=stat){
                oldList = HessianSerialize.deserialize( LibraZk.INSTANCE.client().getData().forPath(proxyPath),List.class);
                for (RpcProvider rpcProvider :oldList) {
                    boolean flag=false;
                    for (RpcProvider newRpc :rpcProviderList) {
                        if (rpcProvider.getInterfaceName().equals(newRpc.getInterfaceName())||
                                rpcProvider.getVersion().equals(newRpc.getVersion())){
                            flag=true;
                            break;
                        }
                    }
                    if (!flag){
                        ZkRegistry.INSTANCE.offline(rpcProvider);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //查ZK之前注册的服务，如果有的在本次找不到，要下线操作
        ZkRegistry.INSTANCE.register(rpcProviderList);//注册代理的服务
        //写zk本次注册的服务
        try {
            LibraZk.INSTANCE.createNode(proxyPath,true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            LibraZk.INSTANCE.client().setData().forPath(proxyPath,HessianSerialize.serialize(rpcProviderList));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  String getProxyNodePath(String service) {
        return String.format("/proxy/%s", service);
    }
}
