package com.nebulauv.libra.proxy.support.util;

import com.nebulauv.libra.common.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;


public class ProxyPropertiesUtil {
    private static final Logger logger = LoggerFactory.getLogger(PropertyUtil.class);
    private static Map<String, String> propertiesMap = new HashMap<>();

    static {
        loadAllProperties();
    }
    private static void loadAllProperties(){
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties("proxy.properties");
            initProperties(properties);
        } catch (IOException e) {
            logger.error("load proxy.properties error");
        }
    }

    public static String getProperty(String name) {
        if (null!=propertiesMap.get(name)){
            return propertiesMap.get(name).toString();
        }
        return  null;

    }

    private static void initProperties( Properties props)  {
        propertiesMap = new HashMap<String, String>();
        for (Object key : props.keySet()) {
            String keyStr = key.toString().trim();
            try {
                propertiesMap.put(keyStr, new String(props.getProperty(keyStr).getBytes("ISO-8859-1"),"utf-8").trim());
            }catch (Exception e){
                logger.error("load proxy.properties error：{}",e);
            };
        }
        logger.info("load proxy.properties：{}",String.valueOf(propertiesMap));
    }

}
