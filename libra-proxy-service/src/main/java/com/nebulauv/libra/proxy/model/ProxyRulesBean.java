package com.nebulauv.libra.proxy.model;

import java.util.List;

/**
 * Created by zhanghui on 2018/4/23.
 */
public class ProxyRulesBean {

    private String proxyServer;

    private List<String> apps;

    public String getProxyServer() {
        return proxyServer;
    }

    public void setProxyServer(String proxyServer) {
        this.proxyServer = proxyServer;
    }

    public List<String> getApps() {
        return apps;
    }

    public void setApps(List<String> apps) {
        this.apps = apps;
    }
}
