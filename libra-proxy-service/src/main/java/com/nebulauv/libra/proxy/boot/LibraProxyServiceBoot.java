package com.nebulauv.libra.proxy.boot;
import com.nebulauv.libra.proxy.thread.LoadProxyServiceThread;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

/**
 * Created by zhanghui on 2018/2/7.
 */
@SpringBootApplication(scanBasePackages = "com.nebulauv.**",
exclude = {MongoAutoConfiguration.class,DataSourceAutoConfiguration.class,HibernateJpaAutoConfiguration.class})
public class LibraProxyServiceBoot {

    public static void main(String argus[]) {
        SpringApplication application = new SpringApplication(LibraProxyServiceBoot.class);
        application.run();
        new Thread(new LoadProxyServiceThread()).start();//发现远程服务并注册，后续改成定时拉取
    }
}
