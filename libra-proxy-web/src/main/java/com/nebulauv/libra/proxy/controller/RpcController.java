package com.nebulauv.libra.proxy.controller;

import com.nebulauv.libra.common.util.HessianSerialize;
import com.nebulauv.libra.proxy.service.ZkRpcService;
import com.nebulauv.libra.register.data.RpcProvider;
import com.nebulauv.libra.remoteing.netty.data.RpcRequest;
import com.nebulauv.libra.remoteing.netty.data.RpcResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;
import java.util.List;

/**
 * Created by zhanghui on 2018/2/7.
 */
@Controller
@RequestMapping("/rpc")
public class RpcController  {
    @Autowired
    private ZkRpcService zkRpcService;

    @RequestMapping("/get/{appName}/service")
    @ResponseBody
    public String rpcService(@PathVariable String appName){
        List<RpcProvider> list=this.zkRpcService.getRpcProviderList(appName);
        String str= null;//将结果序列化
        try {
            str = HessianSerialize.serializeStr(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return  str;
    }

    @RequestMapping("/proxy-service")
    @ResponseBody
    public String proxyService(String data){
        RpcRequest request = null;
        String str=null;
        try {
            request = HessianSerialize.deserializeStr(data,RpcRequest.class);
            String result=this.zkRpcService.runApi(request);
            RpcResponse rpcResponse=new RpcResponse();
            rpcResponse.setRequestId(request.getRequestId());
            rpcResponse.setResult(result);
            str=HessianSerialize.serializeStr(rpcResponse);
        } catch (IOException e) {
            e.printStackTrace();
        }

       return str;
    }
}
