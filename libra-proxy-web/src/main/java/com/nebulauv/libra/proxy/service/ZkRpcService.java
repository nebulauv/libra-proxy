package com.nebulauv.libra.proxy.service;


import com.nebulauv.libra.register.data.RpcProvider;
import com.nebulauv.libra.remoteing.netty.data.RpcRequest;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * Created by zhanghui on 2018/2/7.
 */
public interface ZkRpcService {

    public List<RpcProvider> getRpcProviderList(String appName);

    public String runApi(RpcRequest rpcRequest);
}
