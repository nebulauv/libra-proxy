package com.nebulauv.libra.proxy.service.impl;

import com.alibaba.fastjson.JSON;
import com.nebulauv.libra.common.data.RpcResult;
import com.nebulauv.libra.common.exceptions.NoServerNodeException;
import com.nebulauv.libra.common.reflect.JavaBeanParam;
import com.nebulauv.libra.common.util.HessianSerialize;
import com.nebulauv.libra.common.util.encoder.JsonEncoder;
import com.nebulauv.libra.proxy.service.ZkRpcService;
import com.nebulauv.libra.register.data.RpcMethod;
import com.nebulauv.libra.register.data.RpcParam;
import com.nebulauv.libra.register.data.RpcProvider;
import com.nebulauv.libra.register.zookeeper.LibraZk;
import com.nebulauv.libra.register.zookeeper.ZkDiscovery;
import com.nebulauv.libra.register.zookeeper.ZkPath;
import com.nebulauv.libra.remoteing.netty.client.RpcClient;
import com.nebulauv.libra.remoteing.netty.data.RpcRequest;
import com.nebulauv.libra.remoteing.netty.data.RpcResponse;
import org.apache.commons.collections.map.HashedMap;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.*;

/**
 * Created by zhanghui on 2018/2/7.
 */
@Service("zkRpcService")
public class ZkRpcServiceImpl implements ZkRpcService {
    Logger logger= LoggerFactory.getLogger(ZkRpcServiceImpl.class);
    @Override
    public List<RpcProvider> getRpcProviderList(String appName) {
        List<RpcProvider> ls=new ArrayList<>();
        List<RpcProvider> list=new ArrayList<>();
        String infoDir=ZkPath.getServiceInfoDirPath();
        List<String> serviceInfoList= null;
        try {
            serviceInfoList = LibraZk.INSTANCE.client().getChildren().forPath(infoDir);
            for (String serviceInfo:  serviceInfoList) {
                String path=infoDir+"/"+serviceInfo;
                try{
                    RpcProvider rpcProvider=HessianSerialize.deserialize(LibraZk.INSTANCE.client().getData().forPath(path),RpcProvider.class);
                    list.add(rpcProvider);
                }catch (Exception e){
                    logger.info("读取zk数据异常：path;{},异常：{}",path,e.getMessage());
                }
            }
            //模拟查询
            for (RpcProvider rpcProvider:list) {
                if (rpcProvider.getAppName().equals(appName)){
                    ls.add(rpcProvider);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ls;
    }

    @Override
    public String runApi(RpcRequest rpcRequest) {
        try {
            //api调用
            return callServiceImpl(rpcRequest);
        } catch (Exception e) {
           logger.error("调用API接口失败，异常：{}", e);
           return null;
        }
    }

    private String callServiceImpl(RpcRequest rpcRequest) {
        String service=rpcRequest.getClassName(), version=rpcRequest.getVersion(), method=rpcRequest.getMethodName();
        try{

            RpcProvider rpcProvider= null;
            rpcProvider= ZkDiscovery.INSTANCE.discover(service,version);


            String[] array = rpcProvider.getAddress().split(":");
            String host = array[0];
            int port = Integer.parseInt(array[1]);
            RpcClient client = new RpcClient(host, port); // 初始化 RPC 客户端
            try {
                RpcResponse response = client.send(rpcRequest); // 通过 RPC 客户端发送 RPC 请求并获取 RPC 响应

                //服务端发生异常
                if (response.getError() != null) {
                    logger.error("interface server error!", response.getError());
                    return JsonEncoder.DEFAULT.encode(new RpcResult<String>(2002,"interface server error"));
                }

                Object obj= JsonEncoder.DEFAULT.decode(response.getResult(),Object.class);
                String result=JsonEncoder.DEFAULT.prettyEncode(obj);//美化JSON
                logger.info("invoking interface success：\n {}",result);
                return result;
            } catch (Throwable throwable) {
                logger.error("invoking interface error! {}:{}:{} error:{}", service, version, method, throwable);
                return JsonEncoder.DEFAULT.encode(new RpcResult<String>(2002,"invoking interface error"));
            }
        }catch (NoServerNodeException e){
            logger.error("no server supply {}:{}:{}", service, version, method);
            return JsonEncoder.DEFAULT.encode(new RpcResult<String>(2002,"no server supply"));
        }
    }

    private RpcMethod getRpcMethod(String service, String version, String method) {
        String serviceVersion=service;
        if (!StringUtils.isEmpty(version)){
            serviceVersion=service+"-"+version;
        }
        return this.getRpcMethod(serviceVersion, method);
    }


    public RpcMethod getRpcMethod(String serviceVersion,String method) {
        List<RpcMethod> list=this.getRpcMethodList(serviceVersion);
        for (RpcMethod rpcMethod:list) {
            if(rpcMethod.getMethodName().equals(method)){
                return rpcMethod;
            }
        }
        return null;
    }
    public List<RpcMethod> getRpcMethodList(String serviceVersion){
        String path= ZkPath.getServiceInfoDirPath()+"/"+serviceVersion;
        Map<String,String> map=this.getServerVersionMap(serviceVersion);
        String service=map.get("service");
        String version=map.get("version");
        if(StringUtils.isEmpty(version)){
            path=ZkPath.getServiceInfoDirPath()+"/"+service;
        }
        RpcProvider rpcProvider= null;
        try {
            byte[] bytes = LibraZk.INSTANCE.client().getData().forPath(path);
            rpcProvider =  HessianSerialize.deserialize(bytes,RpcProvider.class);
        } catch (KeeperException.NoNodeException e) {
            throw new NoServerNodeException("no server node error");
        }
        catch (Exception e) {
            throw new NoServerNodeException("no server node error");
        }

        List<RpcMethod> list=rpcProvider.getRpcMethodList();
        return list;
    }

    private Map<String,String> getServerVersionMap(String serviceVersion){
        Map<String,String> map=new HashedMap();
        String service=serviceVersion;
        String version="";
        if (serviceVersion.contains("-")){
            String [] array=serviceVersion.split("-");
            if (array.length>1){
                service=array[0];
                version=array[1];
            }else{
                service=array[0];
            }
        }
        map.put("service",service);
        map.put("version",version);
        return  map;

    }
}
